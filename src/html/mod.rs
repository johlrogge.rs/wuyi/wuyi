pub mod structure;

#[macro_export]
macro_rules! element {
    (@template $c:ident $context:ident $template:expr) => {
        {
            let $context = $c;
            $template
        }
    };

    (@attr $c:ident $attrs:ident $name:ident={$context:ident=>$($template:tt)*}) => {
        {
            let value = element!(@template $c $context $($template)*);
            element!(@attr $c $attrs $name=value)
        }
    };

    (@attr $c:ident $attrs:ident $name:ident={=$template:expr}) => {
        {
            element!(@attr $c $attrs $name=element!(@template $c $c $template($c)))
        }
    };

    (@attr $c:ident $attrs:ident $name:ident=$value:expr, $($tail:tt)*) => (
        element!(@attr $c $attrs $name=$value);
        element!(@attr $c $attrs $($tail)*)
    );

    (@attr $c:ident $attrs:ident $name:ident=$value:expr) => (
        $attrs.push(structure::Attribute{name:stringify!($name), value:$value.into()});
    );

    (@attr $c:ident $attrs:ident)=> {
        ()
    };

    (@attrs $context:ident $($attr_data:tt)*) => {
        {
            let mut attrs:Vec<structure::Attribute> = vec![];
            attrs.clear(); // to get rid of mutable warnings when no attributes are defined
            element!(@attr $context attrs $($attr_data)*);
            attrs
        }
    };

    (@children $context:ident $name:ident($($attrs:tt)*){$($child:tt)*} $($tail:tt)*) => (
        {
            let mut children=vec![element!($context $name ($($attrs)*) {$($child)*}),];
            children.append(&mut element!(@children $context $($tail)*));
            children
        }
    );


    (@children $c:ident {$context:ident => $($template:tt)+} $($tail:tt)*) => (
        {
            let mut children= vec![element!(@template $c $context $($template)+)];
            children.append(&mut element!(@children $c $($tail)*));
            children
        }
    );

    (@children $c:ident {=$template:expr} $($tail:tt)*) => {
        {
            let mut children= vec![element!(@template $c $c $template($c))];
            children.append(&mut element!(@children $c $($tail)*));
            children
        }
    };

    (@children $context:ident $text:expr; $($tail:tt)*) => (
        {
            let mut children= vec![structure::Element::Content($text.into())];
            children.append(&mut element!(@children $context $($tail)*));
            children
        }
    );

    (@children $context:ident $text:expr) => (
        vec![structure::Element::Content($text.into())]
    );

    (@children $context:ident ) => (
        vec![]
    );


    ($context:ident $name:ident ( $($attrs:tt)*) {$($child:tt)*} $($tail:tt)*) => (
        structure::Element::Tag(
                stringify!($name),
                element!(@attrs $context $($attrs)*),
                element!(@children $context $($child)*),
        )
    );


}

#[macro_export]
macro_rules! html {
    ($context:ty => $elem_name:ident ($($attr:tt)*) {$($child:tt)*}) => (
        {
            |_c:$context|
            element!(
                _c
                    $elem_name (
                        $($attr)*
                    )
                {
                    $($child)*
                }
            )
        }
    )
}

#[cfg(test)]
mod test {
    mod basics {
        use html::structure;
        use html::structure::{Attribute, Element, Value};

        #[test]
        fn empty_element() {
            assert_eq!(
                Element::Tag(
                    "html",
                    vec![],
                    vec![],
                ),
                html!{&() =>
                html() {
                }}(&())
            );
        }

        #[test]
        fn element_with_one_attribute() {
            assert_eq!(
                Element::Tag(
                    "li",
                    vec![
                        Attribute {
                            name: "id",
                            value: "Some id".into(),
                        },
                    ],
                    vec![],
                ),
                html!{
                    &() => li (id = "Some id") {}
                }(&())
            )
        }

        #[test]
        fn element_with_text() {
            assert_eq!(
                Element::Tag(
                    "p",
                    vec![],
                    vec![Element::Content(Value::Str("Hello World!")),],
                ),
                html!{
                    &() => p(){"Hello World!"}
                }(&())
            )
        }

        #[test]
        fn element_with_string_expression() {
            assert_eq!(
                Element::Tag(
                    "p",
                    vec![],
                    vec![Element::Content(Value::String("Hello World! 1, 2, 3".to_string())),],
                ),
                html!{
                    &() => p(){format!("Hello World! {}, {}, {}", 1, 2, 3)}
                }(&())
            )
        }
        #[test]
        fn element_with_number_as_text() {
            assert_eq!(
                Element::Tag(
                     "p",
                     vec![],
                     vec![Element::Content(Value::Number(4711)),],
                ),
                html!{
                    &() => p(){4711}
                }(&())
            )
        }


        #[test]
        fn element_with_many_attributes() {
            assert_eq!(
                Element::Tag(
                     "input",
                     vec![
                        Attribute {
                            name: "id",
                            value: "Some id".into(),
                        },
                        Attribute {
                            name: "type",
                            value: "text".into(),
                        },
                        Attribute {
                            name: "disabled",
                            value: "true".into(),
                        },
                    ],
                    vec![],
                ),
                html!{
                    &() => input (id = "Some id", type="text", disabled="true") {}
                }(&())
            )
        }

        #[test]
        fn element_with_sub_element() {
            assert_eq!(
                Element::Tag(
                    "html",
                    vec![],
                    vec![
                        Element::Tag(
                            "body",
                            vec![],
                            vec![],
                        ),
                    ],
                ),
                html!{
                    &() => html() {
                        body() {
                        }
                    }
                }(&())
            )
        }


        #[test]
        fn element_with_mixed_content() {
            assert_eq!(
                Element::Tag(
                     "p",
                     vec![],
                     vec![
                        Element::Content(Value::Str("Hello")),
                        Element::Tag(
                            "strong",
                            vec![],
                            vec![Element::Content(Value::Str("World"))],
                        ),
                    ],
                ),
                html!{
                    &() => p() {"Hello"; strong() {"World"}}
                }(&())
            )
        }

        #[test]
        fn element_with_sub_sibling_elements() {
            assert_eq!(
                Element::Tag(
                     "ul",
                     vec![],
                     vec![
                        Element::Tag(
                             "li",
                             vec![],
                             vec![],
                        ),
                        Element::Tag(
                             "li",
                             vec![],
                             vec![],
                        ),
                    ],
                ),
                html!{
                    &() => ul() {
                        li() {}
                        li() {}
                    }
                }(&())
            )
        }

        #[test]
        fn element_with_sub_sub_element() {
            assert_eq!(
                Element::Tag(
                     "html",
                     vec![],
                     vec![
                        Element::Tag(
                             "body",
                             vec![],
                             vec![
                                Element::Tag(
                                     "br",
                                     vec![],
                                     vec![],
                                ),
                            ],
                        ),
                    ],
                ),
                html!{
                    &() => html() {
                        body() {
                            br(){}
                        }
                    }
                }(&())
            )
        }
    }

    mod parameters {
        use html::structure;
        struct Context {
            value1: &'static str,
        }

        #[test]
        fn attribute_values_from_context_fields() {
            assert_eq!(
                html!{&() => li(id="an_id"){}}(&()),
                html!{&Context => li(id={ctxt=>ctxt.value1}) {}}(&Context { value1: "an_id" })
            )
        }

        #[test]
        fn _values_from_context_fields() {
            use html::structure::Element;
            assert_eq!(
                html!{&() => li(){"some text"}}(&()),
                html!{&Context => li() {{ctxt=>Element::Content(ctxt.value1.into())}}}(&Context { value1: "some text" })
            )
        }
    }

    mod compose {
        use html::structure;
        use html::structure::{Element, Value};

        struct Context {
            value: &'static str,
        }

        #[test]
        fn call_use_external_template_for_attribute() {
            let value_template = |c:&Context|Value::from(c.value);
            let ctxt = Context{ value: "an_id"};
            assert_eq!(
                html!{&Context => li(id={ctxt=>ctxt.value}){}} (&ctxt),
                html!{&Context => li(id={=value_template}){}} (&ctxt)
            )
        }

        #[test]
        fn call_use_external_template_for_content() {
            let content_template = |c:&Context|Element::Content(c.value.into());
            let ctxt = Context{ value: "content"};
            assert_eq!(
                html!{&Context => li(){{ctxt=>Element::Content(Value::from(ctxt.value))}}} (&ctxt),
                html!{&Context => li(){{=content_template}}} (&ctxt)
            )
        }

        #[test]
        fn call_use_external_template_for_sub_element() {
            let content_template = html!{&Context => li(){{ctxt=>Element::Content( ctxt.value.into())}}};
            let ctxt = Context{ value: "content"};
            assert_eq!(
                html!{&Context => ul(){li(){{ctxt=>Element::Content(Value::from(ctxt.value))}}}} (&ctxt),
                html!{&Context => ul(){{=content_template}}} (&ctxt)
            )
        }
    }
}
