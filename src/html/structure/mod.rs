#[derive(PartialEq, Debug)]
pub enum Value<'s> {
    Str(&'s str),
    String(String),
    Number(i32),
    LargeNumber(i64)
}

impl <'s> From<i32> for Value<'s> {
    fn from(v:i32) -> Value<'s> {
        Value::Number(v)
    }
}

impl <'s> From<i64> for Value<'s> {
    fn from(v:i64) -> Value<'s> {
        Value::LargeNumber(v)
    }
}

impl <'s> From<&'s str> for Value<'s> {
    fn from(v:&'s str) -> Value<'s> {
        Value::Str(v)
    }
}

impl <'s> From<String> for Value<'s> {
    fn from(v:String) -> Value<'s> {
        Value::String(v)
    }
}

#[derive(PartialEq, Debug)]
pub struct Attribute<'s> {
    pub name: &'s str,
    pub value: Value<'s>,
}

#[derive(PartialEq, Debug)]
pub enum Element<'s> {
    Tag(&'static str, Vec<Attribute<'s>>, Vec<Element<'s>>),
    Content(Value<'s>)
}

use std::fmt;
use std::fmt::{Display, Formatter, Write};



impl <'s> Display for Value<'s> {
    fn fmt(&self, formatter:&mut Formatter) -> Result<(), fmt::Error> {
        match self {
            Value::LargeNumber(n) => write!(formatter, "{}", n),
            Value::Number(n) => write!(formatter, "{}", n),
            Value::String(s) => write!(formatter, "{}", s),
            Value::Str(s) =>    write!(formatter, "{}", s)
        }
    }
}

impl <'s> Display for Attribute<'s> {
    fn fmt(&self, formatter:&mut Formatter) -> Result<(), fmt::Error> {
        let Attribute{name, value} = self;
        write!(formatter, " {}=\"", name)?;
        write!(formatter, "{}\"", safe_attribute(value))
    }
}

impl <'s> Display for Element<'s> {
    fn fmt(&self, formatter:&mut Formatter) -> Result<(), fmt::Error> {
        match self {
            Element::Content(value) => write!(formatter, "{}", safe_html(&value)),
            Element::Tag(name, attributes, children) => {
                write!(formatter, "<{}", name)?;
                attributes.iter().try_for_each(|a|write!(formatter, "{}", a))?;
                formatter.write_char('>')?;
                children.iter().try_for_each(|e|write!(formatter, "{}", e))?;
                write!(formatter, "</{}>", name)
            },

        }
    }
}

pub fn safe_html(value:&Value) -> impl Display {
    use htmlescape::encode_minimal;
    encode_minimal(&format!( "{}", value))
}

pub fn safe_attribute(value:&Value) -> impl Display {
    use htmlescape::encode_attribute;
    encode_attribute(&format!( "{}", value))
}

#[cfg(test)]
mod test {
    use super::{Element, Attribute, Value, safe_html, safe_attribute};

    #[test]
    fn safe_html_escapes_html() {
        assert_eq!(
            "&quot;This &amp; that&quot;",
            format!("{}", safe_html(&Value::from("\"This & that\"")))
        )
    }

    #[test]
    fn safe_attribute_escapes_html() {
        assert_eq!(
            "&quot;This&#x20;&amp;&#x20;that&quot;",
            format!("{}", safe_attribute(&Value::from("\"This & that\"")))
        )
    }

    #[test]
    fn render_empty_tag() {
        assert_eq!(
            "<ul></ul>",
            format!("{}", Element::Tag("ul", vec![], vec![])))
    }

    #[test]
    fn render_tag_with_empty_sub_tag() {
        assert_eq!(
            "<ul><li></li></ul>",
            format!("{}",
                    Element::Tag("ul", vec![], vec![
                        Element::Tag("li", vec![], vec![]),
                    ])))
    }

    #[test]
    fn render_tag_with_content() {
        assert_eq!(
            format!("<li>{}</li>", safe_html(&"item one".into())),
            format!("{}",
                    Element::Tag("li", vec![], vec![
                        Element::Content("item one".into())
                    ]))
        )
    }

    #[test]
    fn render_tag_with_attribute() {
        assert_eq!(
            format!("<li id=\"{}\"></li>", safe_attribute(&"some_id".into())),
            format!("{}",Element::Tag("li", vec![Attribute{name:"id", value:"some_id".into()}], vec![])))
    }
}
